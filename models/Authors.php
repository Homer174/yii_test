<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "authors".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 */
class Authors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'required'],
            [['firstname', 'lastname'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
        ];
    }

    public static function getList()
    {
        // Выбираем только те категории, у которых есть дочерние категории
        $parents = Authors::find()
            ->select(['authors.id', 'authors.firstname', 'authors.lastname'])
            ->distinct(true)
            ->all();
     
        $firstname = ArrayHelper::map($parents, 'id', 'firstname');
        $lastname = ArrayHelper::map($parents, 'id', 'lastname');
        foreach ($firstname as $key => &$value) {
            $value .= ' '.$lastname[$key];
        }
        return $firstname;
    }
}
