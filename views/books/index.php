<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\models\Authors;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список книг';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("
    $('body').on('beforeFilter', function(event) { return false; });
    $('.btn-success').click(function(){
        $('.gridview-filter-form').submit()
    });", 
    yii\web\View::POS_END);

?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterPosition' => 'header',
        'columns' => [
            [
                'attribute' => 'id',
                'filter' => false
            ],
            'name:ntext:Название',
            [
                'label' => 'Превью',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img('/images/'.$data->preview,[
                        'alt' => $data->name,
                        'style' => 'width:55px;'
                    ]);
                },
            ],
            [
                'label' => 'Автор',
                'attribute' => 'author_id',
                'format' => 'raw',
                'value' => function($data){
                    return $data->author->firstname.' '.$data->author->lastname;
                },
                'filter' => Authors::getList()
            ],
            [
                'label' => 'Дата выхода книги',
                'attribute' => 'date',
                'format' =>  ['date', 'php:d F Y'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'start_date',
                    'attribute2' => 'end_date',
                    'options' => ['placeholder' => 'от'],
                    'options2' => ['placeholder' => 'до'],
                    'type' => DatePicker::TYPE_RANGE,
                    'language' => 'ru',
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                    ]
                ])
 
            ],
            [
                'label' => 'Дата добавления',
                'attribute' => 'date_create',
                'format' =>  ['date', 'php:d F Y'],
                'filter' => Html::submitButton(Yii::t('app', 'Поиск'), ['class' => 'btn btn-success'])
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {view} {delete}',
                'visible' => \Yii::$app->user->identity,
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['target' => '_blank']);
                    },
                    'view' => function ($url, $model, $key) {
                        return \yii\bootstrap\Modal::widget([
                            'id' => 'contact-modal-'.$model->id,
                            'toggleButton' => [
                                'label' => '<span class="glyphicon glyphicon-eye-open"></span>',
                                'tag' => 'a',
                                'data-target' => '#contact-modal-'.$model->id,
                                'href' => $url,
                            ],
                            'clientOptions' => false,
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>